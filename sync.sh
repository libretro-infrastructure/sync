#!/bin/bash

declare -a FAILED_CORE=();

function sync_cores() {
  while read repourl branch; do
    core="$(echo "$repourl" |cut -d/ -f5)"
    core=${core%".git"}
    if [ -z "$branch" ]; then branch="master"; fi

    if [ ! -d $core ]; then
      git clone $repourl;
      git -C $core remote add incom git@gitlab.incom.co:libretro/${core}.git
    else # Track upstream url changes
      git -C $core remote set-url origin $repourl
    fi;

    pushd $core;

    git fetch origin
    git fetch incom
    git checkout incom/${branch}
    if ! git rebase origin/${branch}; then
      git rebase --abort;
      FAILED_CORES+=("${core}");
      popd;
      continue;
    fi;

    if [ "$(git rev-parse --short incom/${branch})" != "$(git rev-parse --short HEAD)" ]; then
      git push --force incom HEAD:refs/heads/${branch}
    fi;

    popd;
  done < $1;
}

sync_cores assets_active.list;
sync_cores common_active.list;
sync_cores content_active.list;
sync_cores cores_active.list;
sync_cores frontends_active.list;
sync_cores packaging_active.list;
sync_cores web_active.list;

if [ "${SYNC_TYPE}" == "full" ]; then
  sync_cores assets_deprecated.list;
  sync_cores common_deprecated.list;
  sync_cores cores_deprecated.list;
  sync_cores frontends_deprecated.list;
  sync_cores infrastructure_deprecated.list;
  sync_cores packaging_deprecated.list;
  sync_cores web_deprecated.list;
  sync_cores unknown.list;
fi;

if [ ! ${#FAILED_CORES[@]} -eq 0 ]; then
  echo "${#FAILED_CORES[@]} failed repos:";
  for core in "${FAILED_CORES[@]}"; do
    echo "  - ${core}"
  done;

  exit 1;
fi;
